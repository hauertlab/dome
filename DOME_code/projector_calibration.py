#This code is provided to facilitate calibration of camera to projector space in the DOME system
#and must be run in parallel with "imaging_calibration.py"  

#authors = Ana Rubio Denniss <am.rubiodenniss@bristol.ac.uk>, Thomas E. Gorochowski & Sabine Hauert
#affiliation = University of Bristol

#This work is licensed under a Creative Commons Attribution 4.0 International License.

################################################################################

import socket
import time
import cv2
import numpy as np
import json

################################################################################

 #FUNCTION TO RECIEVE DATA FROM OTHER RASPBERRY PI NODE

def recieve():
    data = serversocket.recv(1024)
    byteDecode = data.decode(encoding = 'UTF-8')
    return byteDecode

################################################################################

 #FUNCTION TO TRANSMIT DATA TO OTHER RASPBERRY PI NODE

def transmit(transmit_message):
    msg = json.dumps(transmit_message)
    print(msg)
    serversocket.send(bytes(msg,"utf-8"))

################################################################################

 #PROJECTION OF GRID POINTS AROUND PREVIOUSLY LOCATED APPROXIMATE CENTRAL COORDINATES OF THE PROJECTOR SPACE WITH RESPECT TO CAMERA FOV

def grid_projection(projector_centre_height, projector_centre_width, grid_size=grid_size):
    # create a zero array of size specified above with data type 8-bit unsigned integer 
    pattern = np.zeros((display_height, display_width), dtype=np.uint8)

    projector_grid = [[projector_centre_height-grid_size, projector_centre_width-grid_size],[projector_centre_height-grid_size, projector_centre_width+grid_size],
                    [projector_centre_height+grid_size, projector_centre_width-grid_size],[projector_centre_height+grid_size, projector_centre_width+grid_size]]

    for points in projector_grid:
        pattern[points[0], points[1]]=255
        for i in range (-3,5):
            for j in range (-3,4):
                pattern[points[0]+i, points[1]+j]=255

    #writes projector grid coordinates tp a file
    projector_grid_number = 0
    with open('ProGrid_file_.txt', 'w') as f:
      for points in projector_grid:
        projector_grid_number = projector_grid_number +1
        f.write("Grid: %s \n" % projector_grid_number)
        f.write("%s"  % points[1])
        f.write(", %s\n"  % points[0])
        # for subitem in points:
        #   f.write("%s\n"  % subitem)
    #take input camera frame while projecting grid points
    cv2.namedWindow("Pattern", cv2.WND_PROP_FULLSCREEN)
    cv2.setWindowProperty("Pattern",cv2.WND_PROP_FULLSCREEN,cv2.WINDOW_FULLSCREEN)
    cv2.imshow("Pattern", pattern)
    cv2.imwrite("pattern.png", pattern)
    transmit("GRID")
    k=cv2.waitKey(33)
    finish = recieve()
    return

################################################################################

 #DEFINE CALIBRATION PARAMETERS

#grid size dictates size of projected grid, should be smaller for higher magnifications
grid_size = 50
#the number of iteraction of the quadrant search process, should be higher for higher magnifications
iteration_number = 5
#resolution of projector
display_width, display_height = 854, 480
pattern = np.zeros((display_height, display_width), dtype=np.uint8) # create a zero array of size specified above with data type 8-bit unsigned integer 
cv2.namedWindow("Pattern", cv2.WND_PROP_FULLSCREEN)
cv2.setWindowProperty("Pattern",cv2.WND_PROP_FULLSCREEN,cv2.WINDOW_FULLSCREEN)
width = [0, int(display_width/2), int(display_width)]
height = [0, int(display_height/2), int(display_height)]
cv2.imshow("Pattern", pattern)
cv2.waitKey(0)


################################################################################

 #ESTABLISH CLIENT SOCKET TO CONNECT TO IMAGING MODULE AND RUN CALIBRATION ALGORITHM

with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as serversocket:
    serversocket.connect(('192.168.1.2', 65455))
    cv2.imshow("Pattern", pattern)
    k=cv2.waitKey(33)
    for iteration in range (iteration_number):
        width_store = width
        height_store = height

        #flash each quadrant for the state number of iterations, with 

        pattern = np.zeros((display_height, display_width), dtype=np.uint8)
        for j in range (height[0], height[1]):
            for i in range(width[0], width[1]):
                pattern[j,i]=255
        cv2.imshow("Pattern", pattern)
        k=cv2.waitKey(33)
        transmit("PROJECTING Q1")
        recieve_message = recieve()
        print(recieve_message)
        if (recieve_message == 'DETECTED'):
            print("QUADRANT 1 CONFIRMED")
            height[2]=int(height[1])
            height[1]=int((height[2]+ height[0])/2)
            width[2]=int(width[1])
            width[1]=int((width[2]+ width[0])/2)
            continue

        pattern = np.zeros((display_height, display_width), dtype=np.uint8)
        for j in range (height[0], height[1]):
            for i in range(width[1], width[2]):
                pattern[j,i]=255
        cv2.imshow("Pattern", pattern)
        k=cv2.waitKey(33)
        transmit("PROJECTING Q2")
        recieve_message = recieve()
        print(recieve_message)
        if (recieve_message == 'DETECTED'):
            print("QUADRANT 2 CONFIRMED")
            height[2]=int(height[1])
            height[1]=int((height[2]+height[0])/2)
            width[0]=int(width[1])
            width[1]=int((width[2]+width[0])/2)
            continue

        pattern = np.zeros((display_height, display_width), dtype=np.uint8)
        for j in range (height[1], height[2]):
            for i in range(width[0], width[1]):
                pattern[j,i]=255
        cv2.imshow("Pattern", pattern)
        k=cv2.waitKey(33)
        transmit("PROJECTING Q3")
        recieve_message = recieve()
        print(recieve_message)
        if (recieve_message == 'DETECTED'):
            print("QUADRANT 3 CONFIRMED")
            height[0]=int(height[1])
            height[1]=int((height[2]+ height[0])/2)
            width[2]=int(width[1])
            width[1]=int((width[2]+ width[0])/2)
            continue

        pattern = np.zeros((display_height, display_width), dtype=np.uint8)
        for j in range (height[1], height[2]):
            for i in range(width[1], width[2]):
                pattern[j,i]=255
        cv2.imshow("Pattern", pattern)
        k=cv2.waitKey(33)
        transmit("PROJECTING Q4")
        recieve_message = recieve()
        print(recieve_message)
        if (recieve_message == 'DETECTED'):
            print("QUADRANT 4 CONFIRMED")
            height[0]=int(height[1])
            height[1]=int((height[2]+height[0])/2)
            width[0]=int(width[1])
            width[1]=int((width[2]+width[0])/2)
            continue

    print(height, width)
    cv2.imwrite("pattern.png", pattern)
    coordinate_dict = { "height_coordinate": height[1], "width_coordinate": width[1], "grid_size": gridSize}
    transmit(coordinate_dict)
    print(recieve())
    grid_projection(height[1], width[1])
    transmit("NONE")

